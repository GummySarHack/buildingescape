// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "Engine.h"
#include "Components/ActorComponent.h"
#include "Grabber.generated.h"
//#include "Components/InputComponent.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	//How far ahead of the player can we reach in cm
    float reach = 100;
    
    UPhysicsHandleComponent* physicsHandle = nullptr;

    UInputComponent* inputComponent = nullptr;

    //ray-cast and grab
    void Grab();

    void Release();

    void FindPhysicsHandleComponent();

    //setup (assumed) attached input component
    void SetUpInputComponent();

    //return hit for first physics body in reach
    const FHitResult GetFirstPhysicsInReach();

    //returns current start of reach line
    FVector GetReachLineStart();

    //returns current end of reach line
    FVector GetReachLineEnd();
};
