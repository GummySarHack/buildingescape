// Fill out your copyright notice in the Description page of Project Settings.

#include "OpenDoor.h"
#include "GameFramework/Actor.h"
#include "Grabber.h"

#define OUT

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
    bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
    
    ownerDoor = GetOwner();

    initialRotation = ownerDoor->GetActorRotation();
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// pull the trigger volume
     
    if(GetTotalMassOfActorOnPlate() > 50.0f) //TODO
    {
        onOpenRequest.Broadcast();
        lastDoorTime = GetWorld()->GetTimeSeconds();
    }

    //check if time tp close the door
    if(GetWorld()->GetTimeSeconds() - lastDoorTime > doorCloseDelay)
    {
        onCloseRequest.Broadcast();
    }
}

float UOpenDoor::GetTotalMassOfActorOnPlate()
{
    float totalMass = 0.f;

    TArray<AActor*> overLappingActor;
    //find the overlapping actors
        //iterat through them adding their masses
    pressurePlate->GetOverlappingActors(OUT overLappingActor);

    for (const auto& Actor : overLappingActor)
    {
       totalMass += Actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
       UE_LOG(LogTemp, Warning, TEXT("%s on pressure plate"), *Actor->GetName())
    }

    return totalMass;
}
