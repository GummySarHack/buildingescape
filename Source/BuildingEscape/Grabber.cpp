// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"
#include "GameFramework/Actor.h"
#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

    FindPhysicsHandleComponent(); 

    SetUpInputComponent();
}

///look for attached physics handle
void UGrabber::FindPhysicsHandleComponent()
{
    physicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();

    if (physicsHandle == nullptr)
    {
        UE_LOG(LogTemp, Error, TEXT("%s missing physics handle component"), *GetOwner()->GetName());
    }
}

// look for attached input component
void UGrabber::SetUpInputComponent()
{
    inputComponent = GetOwner()->FindComponentByClass<UInputComponent>();

    if (inputComponent)
    {
        inputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
        inputComponent->BindAction("Grab", IE_Released, this, &UGrabber::Release);
    }
    else
    {
        UE_LOG(LogTemp, Error, TEXT("%s missing input component"), *GetOwner()->GetName());

    }
}

void UGrabber::Grab()
{
    auto hitResult = GetFirstPhysicsInReach();
    //try to line trace any actor with collision channel set
    auto ComponentToGrab = hitResult.GetComponent(); // gets the mesh component
    auto ActorHit = hitResult.GetActor();

    ///if we hit something then we attach physics handle
    if (ActorHit)
    {
        if (!physicsHandle) { return; }

        physicsHandle->GrabComponent(
            ComponentToGrab, 
            NAME_None,
            ComponentToGrab->GetOwner()->GetActorLocation(),
            true //allow rotaion
        );
    }
}

void UGrabber::Release()    
{
    if (!physicsHandle) { return; }

    physicsHandle->ReleaseComponent();
    //TODO release physics handle
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    //if the handke is attachedk, 
        //then move the object we're moving
    if (!physicsHandle){return; }

    if (physicsHandle->GrabbedComponent)
    {
        physicsHandle->SetTargetLocation(GetReachLineEnd());
    }
}

const FHitResult UGrabber::GetFirstPhysicsInReach()
{
    ///setup query parameter
    FCollisionQueryParams traceParameters(FName(TEXT("")), false, GetOwner());

    ///Ray-cast to reach distance
    FHitResult HitResult;

    GetWorld()->LineTraceSingleByObjectType(
        OUT HitResult,
        GetReachLineStart(),
        GetReachLineEnd(),
        FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
        traceParameters
    );
    return HitResult;
}

FVector UGrabber::GetReachLineStart()
{
    FVector playerViewPointLocation;
    FRotator playerViewPointRotation;

    GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(OUT playerViewPointLocation,
                                                               OUT playerViewPointRotation);
    return playerViewPointLocation;
}

FVector UGrabber::GetReachLineEnd()
{
    FVector playerViewPointLocation;
    FRotator playerViewPointRotation;

    GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(OUT playerViewPointLocation,
                                                               OUT playerViewPointRotation);
    return playerViewPointLocation + (playerViewPointRotation.Vector() * 100);
}
